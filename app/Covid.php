<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Covid extends Model
{

    use HasTranslations;

    protected $fillable = [
        'title', 'details', 'type','icon'
    ];
    public $translatable = ['details', 'title'];

    protected $table = 'covied';
    protected $appends = ['icon_url'];




    public function getIconUrlAttribute()
        {
	return url('storage/covid_icons/' . $this->icon);
        }


}
