<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Covid;

class CovidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request )
    {
        $data = Covid::when($request->has('type'), function($query) use ($request) {
            $query->where('type', $type);
        })->get();
        return response()->json($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //

        $data = $this->validate($request, [
            'title.en' => 'required|string|max:200',
            'title.ar' => 'required|string|max:200',
            'details.en' => 'required|string|max:1000',
            'details.ar' => 'required|string|max:1000',
            'type' => 'required|string|in:prec,symp',
            'icon' => 'required|image|max:10240',


        ]);
        
        if ($request->hasFile('icon'))
        {
        	$icon = $request->file('icon');
        	$icon->store('/covid_icons', 'public');
        	$data['icon'] = $icon->hashName();
        }

        $covid = Covid::create($data);
        return response()->json($covid);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id )
    {

        $covid = Covid::findOrFail($id);
        $data = $this->validate($request, [
            'title.en' => 'required|string|max:200',
            'title.ar' => 'required|string|max:200',
            'details.en' => 'required|string|max:1000',
            'details.ar' => 'required|string|max:1000',
            'type' => 'required|string|in:prec,symp',
            'icon' => 'sometimes|required|image|max:10240'
        ]);

        if ($request->hasFile('icon'))
        {
            $icon = $request->file('icon');
            $icon->store('/covid_icons', 'public');
            $data['icon'] = $icon->hashName();
        }

        $covid->update($data);

        return response()->json($covid);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $covid = Covid::findOrFail($id);

        $covid->delete();

        return response()->json(['message' => 'Deleted']);


    }
}
