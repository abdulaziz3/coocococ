<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{
    public function index(Request $request )
    {
        $data = News::latest()->paginate(10);
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //

        $data = $this->validate($request, [
            'title' => 'required|string|max:300',
            'details' => 'required|string',
            'icon' => 'required|image|max:10240',
        ]);

        if ($request->hasFile('icon'))
        {
            $icon = $request->file('icon');
            $icon->store('/news_icons', 'public');
            $data['icon'] = $icon->hashName();
        }
        
        $news = News::create($data);
        return response()->json($news);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id )
    {

        $covid = News::findOrFail($id);
        $data = $this->validate($request, [
            'title' => 'required|string|max:200',
            'details' => 'required|string|max:1000',
            'icon' => 'sometimes|required|image|max:10240'
        ]);
        
        if ($request->hasFile('icon'))
        {
        	$icon = $request->file('icon');
        	$icon->store('/news_icons', 'public');
        	$data['icon'] = $icon->hashName();
        }

        $covid->update($data);

        return response()->json($covid);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $covid = News::findOrFail($id);
        $covid->delete();
        return response()->json(['message' => 'Deleted']);
    }
}
