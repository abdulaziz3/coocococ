<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
        'title', 'details','icon'
    ];

    protected $table = 'news';
    protected $appends = ['icon_url'];


    public function getIconUrlAttribute()
    {
        return url('storage/news_icons/' . $this->icon);
    }
}
