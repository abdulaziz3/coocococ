<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Date instrcuter Router
Route::post('/covid', 'CovidController@create');

Route::put('/covid/{id}', 'CovidController@update');

Route::delete('/covid/{id}', 'CovidController@delete');

Route::get('/covid', 'CovidController@index');


// News Router
Route::post('/news', 'NewsController@create');

Route::put('/news/{id}', 'NewsController@update');

Route::delete('/news/{id}', 'NewsController@delete');

Route::get('/news', 'NewsController@index');
